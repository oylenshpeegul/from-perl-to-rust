# Booleans

Rust has a Boolean type (`bool`) and things like `if` and `while` expect things of type `bool` only. Contrast this to Perl where things like `if` and `while` place whatever they're given in *boolean context*. 

Additionally, everything in Rust must be initialized. There are no undefined values. 

So in Perl we have a somewhat complicated set of rules about the *truthiness* and *definedness* of various expressions and values. In Rust, `true` is true, `false` is false, and anything else is going to have to get converted into a Boolean.
