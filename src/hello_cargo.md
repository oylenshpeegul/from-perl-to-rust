# Hello Cargo

In truth, we rarely call the Rust compiler directly like that. Rust comes with a package manager called `cargo` which does all kinds of things for us, including calling `rustc`.

In fact, `cargo` will write the "hello world" program! If we use "cargo new" to create a new project

```
$ cargo new hello
     Created binary (application) `hello` package
```

Then we get a directory called "hello" with a few things already in it

```
$ tree hello
hello
├── Cargo.toml
└── src
    └── main.rs

1 directory, 2 files
```

If we look in that `src/main.rs` file, we see the "hello world" program

```
$ cat hello/src/main.rs 
fn main() {
    println!("Hello, world!");
}
```

If we change to that directory, then "cargo run" will first call the compiler and then run the resulting executable

```
$ cd hello
$ cargo run
   Compiling hello v0.1.0 (/home/tim/hello)
    Finished dev [unoptimized + debuginfo] target(s) in 0.28s
     Running `target/debug/hello`
Hello, world!
```

So this is more like a typical workflow in Rust. Not bad, right? Cargo really takes the rough edges off. It does a whole lot more too, so we'll be seeing it again.

![Cargo run string diagram](cargo_run.svg "cargo run")
