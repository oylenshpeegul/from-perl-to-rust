# Overview

## Tools

As we've just seen, Rust includes some great tooling. Things like `cargo` and `rustup` make it a real pleasure to use. And we'll see more as we go.

| Rust                                                 | Perl                                                   |
|------------------------------------------------------|--------------------------------------------------------|
| `rustc`                                              | `perl`                                                 |
| `cargo`                                              | `cpanm`, `dzil`, and more                              |
| `rustup`                                             | `plenv`, `perlbrew`                                    |
| `rustfmt`                                            | `perltidy`                                             |
| `clippy`                                             | `perlcritic`                                           |
| `rustdoc`                                            | `perldoc`                                              |
| module                                               | module                                                 |
| crate                                                | distribution                                           |
| [crates.io](https://crates.io/)                      | [metacpan.org](https://metacpan.org/)                  |
| [Rust Foundation](https://foundation.rust-lang.org/) | [The Perl Foundation](https://www.perlfoundation.org/) |

We've spent decades coming up with some of this stuff in Perl. In Rust, it's all here already!

## Differences

As we'll see, there are a lot of huge differences between Perl and Rust.

| Rust                         | Perl                    |
|------------------------------|-------------------------|
| static types                 | dynamic types           |
| strong types                 | weak types              |
| move, borrow                 | copy, reference         |
| immutable by default (`mut`) | mutable                 |
| private by default (`pub`)   | public                  |
| expressions                  | statements, expressions |
|                              |                         |

## Similarities

But some things will be familiar. Scope works pretty much the same. Both languages use the `use` keyword and ``::`` in similar ways. And both have lots of "C-like" syntax in common.

