# Ownership

Perl manages our memory for us at run time. Rust does virtually everything at compile time, yet it is still memory safe. How does that work? It does this through its ownership model.

Rust's ownership model is what sets it apart from every other programming language I've used. Earlier, we said Rust's type system was the biggest *practical* difference for Perl programmers. While I believe that's true, Rust's ownership model is the biggest *theoretical* difference. It's what allows Rust to do *so much* at compile time. It's what makes Rust *Rust*!


## Ownership Rules

There are three rules to Rust's ownership

1. Every value has a variable called its owner.
2. There can only be one owner at a time.
3. When the owner goes out of scope, the value is dropped.

So scope determines when values are dropped. That's nice, because earlier we said that Rust's scoping rules work much like Perl's. We're well on our way to understanding already!


