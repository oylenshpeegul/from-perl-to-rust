# Data Races

Rust's ownership model gives memory safety guarantees like "no dangling pointers," and  "no double-frees." It turns out, the things it does to ensure memory safety also prevent data races!

Earlier, we said that--- unlike Perl--- Rust gives us easy access to threads. But programming with multiple threads offers whole new challenges. If one thread wants to change a piece of memory, how do we ensure that no other thread can read it at the same time? Rust's ownership model already does exactly this!

There are other kinds of race conditions possible in Rust, but we don't have to worry about data races. Rust's ownership model is remarkable!

![Ownership Borrowing Lifetimes](ownership.svg)

