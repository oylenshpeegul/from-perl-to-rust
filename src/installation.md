# Installation

If you don't already have Rust installed, we should probably start there. It's not entirely necessary. You could explore for some time on [the Rust Playground](https://play.rust-lang.org/) without installing anything. But eventually, you'll probably want to install it.

## System Rust

One way to install Rust might be through your operating system. I am writing this on a Debian 10 machine, so I could [install Rust](https://packages.debian.org/buster/rustc) with

```
$ sudo apt install rustc
```

or, better, [install Cargo](https://packages.debian.org/buster/cargo) with

```
$ sudo apt install cargo
```

which has `rustc` as a dependency.

## Rustup

Another way to install Rust is with the amazing [rustup](https://rustup.rs/).
Just as we often install and manage multiple versions of Perl with [perlbrew](https://perlbrew.pl/) or [plenv](https://github.com/tokuhirom/plenv), we likely want to do the same for Rust. If we're doing this to ensure we always have the latest version, then it's even more important for Rust than for Perl. They release a new version of stable Perl about once a year. They release a new version of stable Rust every six weeks! That sounds like it could be painful, but it's usually no big deal. We run one command

```
$ rustup update stable
```

and a few seconds later, we have the latest Rust toolchain. I can handle that every six weeks.
