# Next

If you want to learn more, you already have everything you need. The documentation that comes with Rust is truly outstanding. In particular, there is *the book*.

[The Rust Programming Language](https://doc.rust-lang.org/book/) (aka, "the book") is a free guide to the language that comes with it. It is remarkably well done. Whether you read it cover-to-cover or jump around haphazardly, you should definitely read it. You can view a local copy with `rustup docs --book`, even when you're offline. You can also [buy an ink-on-paper copy](https://nostarch.com/rust-programming-language-2nd-edition) if you prefer.

You can see lots of other great docs that come with Rust by running `rustup docs`.


## Other books

- I really liked [Programming Rust: Fast, Safe Systems Development](http://shop.oreilly.com/product/0636920040385.do) by Jim Blandy and Jason Orendorff. I read the first edition, but [now there's a second edition](https://www.oreilly.com/library/view/programming-rust-2nd/9781492052586/) with a third author, [Leonora Tindall](https://nora.codes/).
    - Jim Blandy was on the [CoRecursive](https://corecursive.com/013-rust-and-bitter-c-developers-with-jim-blandy/) podcast [twice](https://corecursive.com/016-moves-and-borrowing-in-rust-with-jim-blandy/).
    - Jason Orendorff gave [a really fun talk](https://www.youtube.com/watch?v=rTo2u13lVcQ) at Rust Belt Rust 2017.
    - Leonora Tindall was on the [Building with Rust](https://anchor.fm/building-with-rust/episodes/Leonora-Tindall-on-Co-Authoring-the-2nd-Edition-of-Programing-Rust-e1d6hok) podcast.

- I started reading [Rust in Action](https://www.manning.com/books/rust-in-action) by Tim McNamara in the electronic early-access version, but it's out now. It takes a project-based approach, which is a little different.

    - Tim McNamara gave [a nice talk](https://www.youtube.com/watch?v=jiBYfpf_M4c) at Linux Conf Australia 2020.
    
