
![Perl to Rust](Perl_to_Rust.jpg "Map from Perl to Rust")

# Introduction

Many introductions to Rust already exist. Most of them are aimed at C++ programmers. That makes sense, but lots of folks are coming to Rust from other languages now.

My current[^note] day job is mostly Perl. It occurred to me that an introduction to Rust aimed at people who already know Perl could be useful.

Rust is exciting to Perl programmers for a number of reasons.
* **We can write faster programs**. Rust is generally more performant than Perl. Often *much* more performant. 
* **We can write more memory-efficient programs**. Rust gives us much greater control over the memory we use than does Perl. Our Perl programs are often memory hogs.
* **We can write multi-threaded programs**. Rust provides easy access to threads. In Perl, we are largely restricted to processes.
* **We can write Perl extension modules in Rust**. Rust interacts with the C [ABI](https://en.wikipedia.org/wiki/Application_binary_interface) really well. If we have a slow Perl module that we were thinking of re-writing in C, we can consider [writing it in Rust](https://github.com/oylenshpeegul/rusty-perl) instead.
* **We can target WebAssembly in Rust**. If we care about web programming, we might be able to use Rust in place of much of our JavaScript. And with the advent of [WASI](https://wasi.dev/), there are more reasons to target WebAssembly; it's not just for the web anymore. For example, with [Krustlet](https://krustlet.dev/) we can substitute WebAssembly artifacts for Docker containers in the Cloud. And now there is [Spin](https://developer.fermyon.com/spin/index)!

Rust is a big, complicated language. Where do we begin? It feels like we have to know everything at once. As [Lisa Passing put it](https://www.youtube.com/watch?v=Ve8HUfU3ELQ&t=5m55s), the learning curve can be more like a wall you hit. But she goes on to say, "once you've learned how to walk through this wall...you can walk through walls."

*I want to walk through walls!*

Having to know everything at once makes it hard to teach Rust as well. It seems like no matter where we start, we are always touching on concepts that we haven't covered yet. This is quite the opposite of Perl, where it's fairly easy to learn as we go. But perhaps making this one assumption--- that we all know Perl--- will help us navigate the complexities of Rust. I don't know if this is going to work, but I thought I'd try it.

-- [Tim Heaney](https://oylenshpeegul.gitlab.io/blog/)

___
[^note]: Update: I no longer have this job. If you would like to hire me, [please get in touch!](https://www.linkedin.com/in/timothyheaney/)
