# Documentation

Rust uses [markdown](https://commonmark.org/) pretty much everywhere that we would use POD in Perl.

| Rust         | Perl          |
|--------------|---------------|
| `//`         | `#`           |
| `//!`, `///` | `=pod`        |
| markdown     | POD           |
| doctests     | Test::Doctest |
| rustdoc      | perldoc       |

Rust uses `//` for regular line comments, but `///` for markdown comments. We can even use markdown's triple backticks ` ``` ` to include Rust source code. When we do this, it automatically becomes a doctest! That is, `cargo test --doc` runs the code we include in markdown comments. This helps us keep our docs up to date (`cargo test` includes `--doc` by default). I think perhaps the worst error in Perl is a `SYNOPSIS` section in our POD that doesn't work because the code changed, but the POD didn't. It's annoyingly easy to do.

For example, here is [the `apfs` function](https://github.com/oylenshpeegul/Epochs-rust/blob/master/src/lib.rs#L13-L23) from the [epochs](https://crates.io/crates/epochs) crate.

```rust
/// APFS time is the number of nanoseconds since the Unix epoch
/// (*cf.*, [APFS filesystem format](https://blog.cugu.eu/post/apfs/)).
///
/// ```
/// use epochs::apfs;
/// let ndt = apfs(1_234_567_890_000_000_000).unwrap();
/// assert_eq!(ndt.to_string(), "2009-02-13 23:31:30");
/// ```
pub fn apfs(num: i64) -> Option<NaiveDateTime> {
    epoch2time(num, 1_000_000_000, 0)
}
```

It contains a description of the function as well as a doctest. The compiler itself ignores it, like any other comment, but `rustdoc` will grab that description and run that test. Currently, rustdoc only generates HTML output that we must then view in a browser. There is no command line tool for looking up things in the docs like perldoc yet.

I've documented all of my functions in a similar way, so rustdoc generates [this whole page](https://docs.rs/epochs/) for me. I didn't write that page, it was all generated from the source code when I uploaded it to [crates.io](https://crates.io/)!

Now, `///` documents whatever immediately follows it, like the function above. There is also `//!` which goes after. That [summary line](https://github.com/oylenshpeegul/Epochs-rust/blob/master/src/lib.rs#L1) for the whole crate came from one of those at the top.
